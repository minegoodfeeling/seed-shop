package com.mgfallen.randomseedshop.models;

public enum Status {
    ACTIVE,
    BANNED;
}
