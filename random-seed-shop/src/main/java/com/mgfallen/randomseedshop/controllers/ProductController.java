package com.mgfallen.randomseedshop.controllers;

import com.mgfallen.randomseedshop.entity.Product;
import com.mgfallen.randomseedshop.entity.ProductType;
import com.mgfallen.randomseedshop.repo.ProductRepository;
import com.mgfallen.randomseedshop.repo.ProductTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Optional;

@Controller
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductTypeRepository productTypeRepository;


    @GetMapping("/product")
    public String seedList(Model model) {
        Iterable<Product> product = productRepository.findAll();
        model.addAttribute("products", product);
        return "product-main";
    }

    @GetMapping("/product/add")
    @PreAuthorize("hasAuthority('product:write')")
    public String seedAdd(Model model) {
        return "product-add";
    }

    @PostMapping("/product/add")
    @PreAuthorize("hasAuthority('product:write')")
    public String seedPostAdd(@RequestParam String name,
                              @RequestParam int price,
//                              @RequestParam String image,
                              @RequestParam String specification,
//                              @RequestParam ProductType productType,
                              Model model) {
        Product product = new Product(name, price, specification);
        productRepository.save(product);
        return "redirect:/product";

    }

    @GetMapping("/product/{id}")
    @PreAuthorize("hasAuthority('product:read')")
    public String seedDetails(@PathVariable(value = "id") Long id,
                              Model model) {
        if(!productRepository.existsById(id)) {
            return "redirect:/product";
        }
        Optional<Product> product = productRepository.findById(id);
        ArrayList<Product> productArrayList = new ArrayList<>();
        product.ifPresent(productArrayList::add);
        model.addAttribute("products", productArrayList);
        return "product-details";
    }

    @GetMapping("/product/{id}/edit")
    @PreAuthorize("hasAuthority('product:write')")
    public String seedEdit(@PathVariable(value = "id") Long id,
                              Model model) {
        if(!productRepository.existsById(id)) {
            return "redirect:/product";
        }
        Optional<Product> product = productRepository.findById(id);
        ArrayList<Product> productArrayList = new ArrayList<>();
        product.ifPresent(productArrayList::add);
        model.addAttribute("products", productArrayList);
        return "product-edit";
    }

    @PostMapping("/product/{id}/edit")
    @PreAuthorize("hasAuthority('product:write')")
    public String seedPostUpdate(@RequestParam String name,
                              @RequestParam int price,
                              @RequestParam String specification,
//                              @RequestParam String image,
//                              @RequestParam ProductType productType,
                              @PathVariable(value = "id") Long id,
                              Model model) {
        Product product = productRepository.findById(id).orElseThrow();
        product.setName(name);
        product.setPrice(price);
        product.setSpecification(specification);
//        product.setProductType(productType);
//        product.setImage(image);


        productRepository.save(product);
        return "redirect:/product";
    }

    @PostMapping("/product/{id}/remove")
    @PreAuthorize("hasAuthority('product:write')")
    public String seedPostDelete(@PathVariable(value = "id") Long id,
                                 Model model) {
        Product product = productRepository.findById(id).orElseThrow();
        productRepository.delete(product);
        return "redirect:/product";
    }


}
