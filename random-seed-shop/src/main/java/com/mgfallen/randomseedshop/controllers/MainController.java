package com.mgfallen.randomseedshop.controllers;

import com.mgfallen.randomseedshop.entity.Product;
import com.mgfallen.randomseedshop.entity.ProductType;
import com.mgfallen.randomseedshop.repo.ProductRepository;
import com.mgfallen.randomseedshop.repo.ProductTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping("/")
    public String home(Model model) {
        Iterable<Product> products = productRepository.findAll();
        model.addAttribute("products", products);
        return "home";
    }

    @GetMapping("/about")
    public String name(Model model) {
        model.addAttribute("title","Страница про нас");
        return "about";
    }

}