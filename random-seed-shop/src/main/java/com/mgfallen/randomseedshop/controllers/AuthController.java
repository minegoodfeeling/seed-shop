package com.mgfallen.randomseedshop.controllers;

import com.mgfallen.randomseedshop.entity.User;
import com.mgfallen.randomseedshop.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private UserRepository userRepository;


    @GetMapping("/login")
    public String getLoginPage() {
        return "login";
    }

    @GetMapping("/registration")
    public String getRegPage() { return "registration"; }

    @PostMapping("/registration")
    public String addUser(User user, Map<String, Object> model) {
        Optional<User> userFromDb = userRepository.findByEmail(user.getEmail());

        if(userFromDb.isPresent()) {
            model.put("message", "User exists");
            return "registration";
        }


        return "redirect:/login";
    }

}
