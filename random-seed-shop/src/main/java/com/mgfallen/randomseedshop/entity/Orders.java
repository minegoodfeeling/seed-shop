package com.mgfallen.randomseedshop.entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Data
@Entity(name = "orders")
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double amount;

    @Column(name = "user_address")
    private String customerAddress;

    @Column(name = "user_email", length = 128)
    private String userEmail;

    @Column(name = "oderd_date")
    private LocalDateTime orderDate;

    public Orders() {

    }
}
