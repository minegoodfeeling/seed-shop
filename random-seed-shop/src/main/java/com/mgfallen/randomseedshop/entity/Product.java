package com.mgfallen.randomseedshop.entity;

import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Data
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private int price;
//    private String image;

    @Type(type = "text")
    private String specification;

//    @OneToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "producttype_id")
//    private ProductType productType;

    public Product(String name, int price, String specification) {
        this.name = name;
        this.price = price;
//        this.image = image;
        this.specification = specification;
//        this.productType = productType;
    }

    public Product(){

    }
}
