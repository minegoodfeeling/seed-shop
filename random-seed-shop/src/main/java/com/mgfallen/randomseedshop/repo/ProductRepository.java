package com.mgfallen.randomseedshop.repo;

import com.mgfallen.randomseedshop.entity.Product;
import com.mgfallen.randomseedshop.entity.ProductType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Long> {
//    public List<Product> findByProductType(Product product);
}
