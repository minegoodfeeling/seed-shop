package com.mgfallen.randomseedshop.repo;

import com.mgfallen.randomseedshop.entity.ProductType;
import org.springframework.data.repository.CrudRepository;

public interface ProductTypeRepository extends CrudRepository<ProductType, Long> {

}
